'use strict';

/**
 * @ngdoc function
 * @name notesApp.controller:NotesCtrl
 * @description
 * # NotesCtrl
 * Controller of the notesApp
 */
angular.module('notesApp')
  .controller('NotesCtrl', function ($scope, $http, $location, authenticationService, noteService, ngNotify) {

    $scope.full = false;  // show or hide full note text (ng-repeat will use it in all notes as single note)

    $scope.activate = function () {
      if (!authenticationService.isAuthenticated()) {
        $location.url('/login');
      }
    };

    $scope.getNotes = function () {
      $http.get('/api/v1/notes', {}).then(
        function (data) {
          $scope.notes = data;
        },
        function () {
          if (authenticationService.isAuthenticated()) {
            ngNotify.set('Cannot retrieve notes!', {
              theme: 'pastel',
              position: 'bottom',
              duration: 1000,
              type: 'error',
              sticky: false,
              html: false
            });
          }
        });
    };

    $scope.removeNote = function (note) {
      noteService.remove(note.id, function () {
        $scope.notes.data.splice($scope.notes.data.indexOf(note), 1);
      }, function () {
        ngNotify.set('Cannot remove note!', {
          theme: 'pastel',
          position: 'bottom',
          duration: 1000,
          type: 'error',
          sticky: false,
          html: false
        });
      });
    };

    $scope.activate();
    $scope.getNotes();
  });
