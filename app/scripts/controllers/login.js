'use strict';

/**
 * @ngdoc function
 * @name notesApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the notesApp
 */
angular.module('notesApp')
  .controller('LoginCtrl', function ($location, $scope, authenticationService, ngNotify) {

    $scope.username = '';
    $scope.password = '';

    $scope.submitted = false;

    $scope.activate = function() {
      if (authenticationService.isAuthenticated()) {
        $location.url('/notes');
      }
    };

    $scope.submitForm = function(isValid) {
      $scope.submitted = true;

      if (isValid) {
        $scope.logIn();
      }
    };

    $scope.logIn = function () {
      authenticationService.login($scope.username, $scope.password, function() {
        $location.url('/notes');
      }, function() {
        ngNotify.set('Cannot login with provided credentials!', {
          theme: 'pastel',
          position: 'bottom',
          duration: 1000,
          type: 'error',
          sticky: false,
          html: false
        });
      });
    };

    $scope.activate();
  });
