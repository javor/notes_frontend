'use strict';

/**
 * @ngdoc function
 * @name notesApp.controller:NewnoteCtrl
 * @description
 * # NewnoteCtrl
 * Controller of the notesApp
 */
angular.module('notesApp')
  .controller('NewnoteCtrl', function ($scope, $location, authenticationService, noteService, ngNotify) {

    $scope.content = '';

    $scope.activate = function () {
      if (!authenticationService.isAuthenticated()) {
        $location.url('/login');
      }
    };

    $scope.addNote = function () {
      noteService.create($scope.content, function () {
        $scope.content = '';

        ngNotify.set('Successful added note!', {
          theme: 'pastel',
          position: 'bottom',
          duration: 1000,
          type: 'success',
          sticky: false,
          html: false
        });
      }, function () {
        ngNotify.set('Fail during adding note!', {
          theme: 'pastel',
          position: 'bottom',
          duration: 1000,
          type: 'error',
          sticky: false,
          html: false
        });
      });
    };

    $scope.activate();
  });
