'use strict';

/**
 * @ngdoc function
 * @name notesApp.controller:NavbarCtrl
 * @description
 * # NavbarCtrl
 * Controller of the notesApp
 */
angular.module('notesApp')
  .controller('NavbarCtrl', function ($scope, authenticationService) {

    $scope.logOut = function () {
      authenticationService.logout();
    };

    $scope.isAuthenticated = function () {
      return authenticationService.isAuthenticated();
    };
  });
