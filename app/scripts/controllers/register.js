'use strict';

/**
 * @ngdoc function
 * @name notesApp.controller:RegisterCtrl
 * @description
 * # RegisterCtrl
 * Controller of the notesApp
 */
angular.module('notesApp')
  .controller('RegisterCtrl', function ($location, $scope, authenticationService, ngNotify) {

    $scope.submitted = false;

    $scope.submitForm = function(isValid) {
      $scope.submitted = true;

      if (isValid) {
        $scope.register();
      }
    };

    $scope.register = function() {
      authenticationService.register($scope.email, $scope.password, $scope.username, function() {
        $scope.email = $scope.password = $scope.username = '';
        $location.url('/login');

        ngNotify.set('Successful created new account!', {
          theme: 'pastel',
          position: 'bottom',
          duration: 1000,
          type: 'success',
          sticky: false,
          html: false
        });
      }, function() {
        ngNotify.set('Cannot create account with provided credentials!', {
          theme: 'pastel',
          position: 'bottom',
          duration: 1000,
          type: 'error',
          sticky: false,
          html: false
        });
      });
    };
  });
