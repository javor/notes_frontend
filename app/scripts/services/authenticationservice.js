'use strict';

/**
 * @ngdoc service
 * @name notesApp.authenticationService
 * @description
 * # authenticationService
 * Service in the notesApp.
 */
angular.module('notesApp')
  .service('authenticationService', function ($cookies, $http) {

    function getAuthenticatedAccount() {
      if (!$cookies.authenticatedAccount) {
        return;
      }
      return JSON.parse($cookies.authenticatedAccount);
    }

    function isAuthenticated() {
      return !!$cookies.authenticatedAccount;
    }

    function setAuthenticatedAccount(account) {
      $cookies.authenticatedAccount = JSON.stringify(account);
    }

    function unauthenticate() {
      delete $cookies.authenticatedAccount;
    }

    function login(username, password, successCallback, errorCallback) {
      return $http.post('/api/v1/auth/login/', {
        username: username, password: password
      }).
        success(function (data, status, headers, config) {
          setAuthenticatedAccount(data);
          successCallback(data, status, headers, config);
        }).error(errorCallback);
    }

    function register(email, password, username, successCallback, errorCallback) {
      return $http.post('/api/v1/accounts/', {
        email: email,
        username: username,
        password: password
      }).success(successCallback).error(errorCallback);
    }

    function logout() {
      return $http.post('/api/v1/auth/logout/', {})
        .success(function () {
          unauthenticate();
          window.location = '/';
        }).error(function () {
          console.error('Epic failure!');
        });
    }

    return {
      getAuthenticatedAccount: getAuthenticatedAccount,
      isAuthenticated: isAuthenticated,
      login: login,
      logout: logout,
      register: register,
      setAuthenticatedAccount: setAuthenticatedAccount,
      unauthenticate: unauthenticate
    };
  });
