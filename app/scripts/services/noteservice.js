'use strict';

/**
 * @ngdoc service
 * @name notesApp.noteService
 * @description
 * # noteService
 * Service in the notesApp.
 */
angular.module('notesApp')
  .service('noteService', function ($http) {
    var create = function (content, successCallback, errorCallback) {
      return $http.post('/api/v1/notes/', {content: content})
        .success(successCallback).error(errorCallback);
    };

    var remove = function (pk, successCallback, errorCallback) {
      return $http({method: 'DELETE', url: '/api/v1/notes/' + pk})
        .success(successCallback).error(errorCallback);
    };

    return {
      create: create,
      remove: remove
    };
  });
