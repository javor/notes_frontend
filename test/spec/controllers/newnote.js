'use strict';

describe('Controller: NewnoteCtrl', function () {

  // load the controller's module
  beforeEach(module('notesApp'));

  var NewnoteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewnoteCtrl = $controller('NewnoteCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewnoteCtrl.awesomeThings.length).toBe(3);
  });
});
